module Puppet::Parser::Functions
  newfunction(:new, :type => :rvalue) do |args|
      if `rpm -qa ntp`.empty?
         then
           ntp_installed = "NO"
         else
           ntp_installed = "YES"
         end
          return ntp_installed
  end
end
