class ntp::files {
    file {'/dev/shm/ntp':
      ensure => present,
      source => "puppet:///modules/ntp/sample",
}
file {'/dev/shm/template':
   ensure => present,
   content =>template("ntp/temp.erb"),
}
}
