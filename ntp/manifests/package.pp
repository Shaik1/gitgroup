class ntp::package {
   $ntp_installed = new()
     if $ntp_installed == "NO" {
        package {'ntp':
        ensure => installed,
        }
       }
     else {
       notice("package ntp was already installed")
     }
}
